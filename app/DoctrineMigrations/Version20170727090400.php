<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170727090400 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE jam_jar_type');
        $this->addSql('DROP TABLE jam_jar_year');
        $this->addSql('ALTER TABLE jam_jar ADD year_id INT DEFAULT NULL, ADD type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE jam_jar ADD CONSTRAINT FK_463B82240C1FEA7 FOREIGN KEY (year_id) REFERENCES property (id)');
        $this->addSql('ALTER TABLE jam_jar ADD CONSTRAINT FK_463B822C54C8C93 FOREIGN KEY (type_id) REFERENCES property (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_463B82240C1FEA7 ON jam_jar (year_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_463B822C54C8C93 ON jam_jar (type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE jam_jar_type (jam_jar_id INT NOT NULL, property_id INT NOT NULL, INDEX IDX_9CC18D5EA52F43E8 (jam_jar_id), INDEX IDX_9CC18D5E549213EC (property_id), PRIMARY KEY(jam_jar_id, property_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE jam_jar_year (jam_jar_id INT NOT NULL, property_id INT NOT NULL, INDEX IDX_AB9DA940A52F43E8 (jam_jar_id), INDEX IDX_AB9DA940549213EC (property_id), PRIMARY KEY(jam_jar_id, property_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE jam_jar_type ADD CONSTRAINT FK_9CC18D5E549213EC FOREIGN KEY (property_id) REFERENCES property (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE jam_jar_type ADD CONSTRAINT FK_9CC18D5EA52F43E8 FOREIGN KEY (jam_jar_id) REFERENCES jam_jar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE jam_jar_year ADD CONSTRAINT FK_AB9DA940549213EC FOREIGN KEY (property_id) REFERENCES property (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE jam_jar_year ADD CONSTRAINT FK_AB9DA940A52F43E8 FOREIGN KEY (jam_jar_id) REFERENCES jam_jar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE jam_jar DROP FOREIGN KEY FK_463B82240C1FEA7');
        $this->addSql('ALTER TABLE jam_jar DROP FOREIGN KEY FK_463B822C54C8C93');
        $this->addSql('DROP INDEX UNIQ_463B82240C1FEA7 ON jam_jar');
        $this->addSql('DROP INDEX UNIQ_463B822C54C8C93 ON jam_jar');
        $this->addSql('ALTER TABLE jam_jar DROP year_id, DROP type_id');
    }
}
