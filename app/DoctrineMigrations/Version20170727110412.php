<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170727110412 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE jam_jar DROP INDEX UNIQ_463B82240C1FEA7, ADD INDEX IDX_463B82240C1FEA7 (year_id)');
        $this->addSql('ALTER TABLE jam_jar DROP INDEX UNIQ_463B822C54C8C93, ADD INDEX IDX_463B822C54C8C93 (type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE jam_jar DROP INDEX IDX_463B82240C1FEA7, ADD UNIQUE INDEX UNIQ_463B82240C1FEA7 (year_id)');
        $this->addSql('ALTER TABLE jam_jar DROP INDEX IDX_463B822C54C8C93, ADD UNIQUE INDEX UNIQ_463B822C54C8C93 (type_id)');
    }
}
