<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BatteryPack;
use AppBundle\Form\BatteryPackType;
use AppBundle\Repository\BatteryPackRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BatteryPackController
 * @package AppBundle\Controller
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 *
 * @Route("/battery-pack")
 */
class BatteryPackController extends Controller
{
    /**
     * @Route("/", name="app_battery_pack_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $batteryPacks = $this->getRepository()->findAll();

        return $this->render('battery-pack/index.html.twig', [
            'batteryPacks' => $batteryPacks,
        ]);
    }

    /**
     * @Route("/view/{id}", name="app_battery_pack_show")
     * @Method("GET")
     * @ParamConverter("batteryPack", class="AppBundle:BatteryPack")
     */
    public function showAction(BatteryPack $batteryPack)
    {
        return $this->render('battery-pack/show.html.twig', [
            'batteryPack' => $batteryPack,
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/new", name="app_battery_pack_new")
     *
     * @return Response
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(BatteryPackType::class)
            ->add('submit', SubmitType::class, ['label' => 'battery_pack.form.label.submit']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->getRepository()->add($data);

            $this->addFlash('success', 'battery_pack.flashes.success');

            return $this->redirectToRoute('app_battery_pack_show', [
                'id' => $data->getId(),
            ]);
        }

        return $this->render('battery-pack/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return \AppBundle\Service\StatisticsCollector
     */
    public function getStatisticsService()
    {
        return $this->get('app.service.statistics');
    }

    /**
     * @return \Doctrine\ORM\EntityRepository|BatteryPackRepository
     */
    public function getRepository()
    {
        return $this->get('app.repository.battery_pack');
    }
}
