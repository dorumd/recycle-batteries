<?php

namespace AppBundle\DTO;

use AppBundle\Entity\Enum\BatteryTypeEnum;

/**
 * Class Statistics
 * @package AppBundle\DTO
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class Statistics
{
    /** @var array */
    protected $data;

    /**
     * Statistics constructor.
     */
    public function __construct()
    {
        $this->data = [];

        foreach (BatteryTypeEnum::ELEMENTS as $type => $label) {
            $this->data[$type] = [
                'label' => $label,
                'count' => 0,
            ];
        }
    }

    /**
     * Add stat
     *
     * @param int $type
     * @param int $count
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function add(int $type, int $count)
    {
        if (!isset($this->data[$type])) {
            throw new \Exception('Invalid type');
        }

        $this->data[$type]['count']+= $count;

        return $this;
    }

    /**
     * Remove stat
     *
     * @param int $type
     * @param int $count
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function remove(int $type, int $count)
    {
        if (!isset($this->data[$type])) {
            throw new \Exception('Invalid type');
        }

        $this->data[$type]['count']-= $count;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
