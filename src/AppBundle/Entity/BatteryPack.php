<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Enum\BatteryTypeEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BatteryPack
 *
 * @ORM\Table(name="battery_pack")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BatteryPackRepository")
 */
class BatteryPack
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="count", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(value="0")
     */
    private $count;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * BatteryPack constructor.
     */
    public function __construct()
    {
        $this->count = 0;
        $this->type = BatteryTypeEnum::TYPE_AA;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return BatteryPack
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return BatteryPack
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BatteryPack
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

