<?php

namespace AppBundle\Entity\Enum;

/**
 * Class BatteryTypeEnum
 * @package AppBundle\Entity\Enum
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class BatteryTypeEnum
{
    const TYPE_AA = 0;
    const TYPE_AAA = 1;
    const TYPE_C = 2;
    const TYPE_D = 3;
    const TYPE_9V = 4;

    const ELEMENTS = [
        self::TYPE_AA => 'battery.type.aa',
        self::TYPE_AAA => 'battery.type.aaa',
        self::TYPE_C => 'battery.type.c',
        self::TYPE_D => 'battery.type.d',
        self::TYPE_9V => 'battery.type.9v',
    ];
}
