<?php

namespace AppBundle\Features\Context;

use AppBundle\Entity\BatteryPack;
use AppBundle\Entity\Enum\BatteryTypeEnum;
use AppBundle\Repository\BatteryPackRepository;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class FeatureContext
 * @package AppBundle\Features\Context
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var KernelInterface */
    private $kernel;

    /**
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Given /^I wait for (\d+) seconds$/
     */
    public function iWaitForSeconds($seconds)
    {
        $this->getSession()->wait($seconds * 1000);
    }

    /**
     * @Given I should be on view page for battery pack :name
     */
    public function iShouldBeOnViewPageForBatteryPack($name)
    {
        $batteryPack = $this->getBatteryPackRepository()->findOneBy(['name' => $name]);
        \PHPUnit_Framework_Assert::assertNotEmpty($batteryPack);

        $this->assertPageAddress($this->getContainer()->get('router')->generate('app_battery_pack_show', [
            'id' => $batteryPack->getId()
        ]));
    }

    /**
     * @Given there are :count :type battery packs
     */
    public function thereAreBatteryPacks($count, $type)
    {
        $repository = $this->getBatteryPackRepository();

        $batteryPack = new BatteryPack();
        $batteryPack->setCount((int) $count);
        $batteryPack->setType(constant(BatteryTypeEnum::class.'::TYPE_'.$type));
        $batteryPack->setName('Example');

        $repository->add($batteryPack);
    }

    /**
     * @BeforeScenario
     */
    public function clearData()
    {
        $purger = new ORMPurger($this->getContainer()->get('doctrine')->getManager());
        $purger->purge();
    }

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @return \Doctrine\ORM\EntityRepository|BatteryPackRepository
     */
    private function getBatteryPackRepository()
    {
        return $this->getContainer()->get('app.repository.battery_pack');
    }
}

