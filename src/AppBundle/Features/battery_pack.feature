Feature: In order, to see batteries statistics
  I should be able to create battery packs

  Scenario: Create battery pack
    Given I am on "/en/battery-pack/new"
    When I fill in "battery_pack[name]" with "Battery Pack #1"
    And I select "AAA" from "battery_pack[type]"
    And I fill in "battery_pack[count]" with "10"
    When I press "Submit"
    Then I should be on view page for battery pack "Battery Pack #1"
