Feature: Statistics

  Background:
    Given there are "2" "AAA" battery packs
    And there are "1" "AAA" battery packs
    And there are "2" "AA" battery packs
    And there are "2" "AA" battery packs
    And there are "1" "AA" battery packs

  Scenario: View statistics
    Given I am on "/en/battery-pack"
    Then I should see "AA: 5"
    And I should see "AAA: 3"
