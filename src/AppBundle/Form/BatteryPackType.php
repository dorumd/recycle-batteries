<?php

namespace AppBundle\Form;

use AppBundle\Entity\BatteryPack;
use AppBundle\Entity\Enum\BatteryTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BatteryPackType
 * @package AppBundle\Form\Type
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class BatteryPackType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'battery_pack.form.label.name',
                'required' => false,
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'battery_pack.form.label.type',
                'choices' => array_flip(BatteryTypeEnum::ELEMENTS),
                'required' => true,
            ])
            ->add('count', IntegerType::class, [
                'label' => 'battery_pack.form.label.count',
                'required' => true,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BatteryPack::class,
        ]);
    }
}