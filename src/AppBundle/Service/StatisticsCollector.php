<?php

namespace AppBundle\Service;

use AppBundle\DTO\Statistics;
use AppBundle\Entity\BatteryPack;
use AppBundle\Repository\BatteryPackRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class StatisticsCollector
 * @package AppBundle\Service
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class StatisticsCollector
{
    /** @var EntityRepository|BatteryPackRepository */
    protected $batteriesPackRepository;

    /** @var Statistics */
    protected $statistics;

    /** @var bool */
    protected $collected;

    /**
     * StatisticsCollector constructor.
     * @param EntityRepository $batteriesPackRepository
     */
    public function __construct(EntityRepository $batteriesPackRepository)
    {
        $this->batteriesPackRepository = $batteriesPackRepository;
        $this->statistics = new Statistics();
        $this->collected = false;
    }

    /**
     * Collect batteries statistics
     *
     * @return Statistics
     *
     * @throws \Exception
     */
    public function collect(): Statistics
    {
        /** @var [] $data */
        $data = $this->batteriesPackRepository->findCountOfType();

        foreach ($data as $value) {
            $this->statistics->add($value['type'], (int) $value['totalAmount']);
        }

        $this->collected = true;

        return $this->statistics;
    }

    /**
     * @return bool
     */
    public function isCollected()
    {
        return $this->collected;
    }

    /**
     * @return array
     *
     * @throws \Exception
     */
    public function getCollectedData(): array
    {
        if (!$this->collected) {
            throw new \Exception('Statistics we\'re not collected.');
        }

        return $this->statistics->getData();
    }
}
