<?php

namespace AppBundle\Twig;

use AppBundle\Service\StatisticsCollector;

/**
 * Class StatisticsExtension
 * @package AppBundle\Twig
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class StatisticsExtension extends \Twig_Extension
{
    /**
     * @var StatisticsCollector
     */
    protected $statisticsCollector;

    /**
     * StatisticsExtension constructor.
     *
     * @param StatisticsCollector $statisticsCollector
     */
    public function __construct(StatisticsCollector $statisticsCollector)
    {
        $this->statisticsCollector = $statisticsCollector;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('renderStatistics', [$this, 'renderStatistics'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }

    /**
     * @param \Twig_Environment $environment
     *
     * @return string
     *
     * @throws \Exception
     */
    public function renderStatistics(\Twig_Environment $environment)
    {
        return $environment->render('common/_statistics.html.twig', [
            'statistics' => $this->statisticsCollector->getCollectedData(),
        ]);
    }
}
