<?php

namespace JamBundle\Admin;

use JamBundle\Entity\Property;
use JamBundle\Repository\PropertyRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class JamJarAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        /** @var PropertyRepository $repo */
        $repo = $em->getRepository(Property::class);

        $form->add('comment', 'text', ['required' => false]);
        $form->add('year', 'choice', [
            'choices' => $repo->findBy(['type' => Property::TYPE_YEAR]),
            'choice_label' => function (Property $property) {
                return $property->getValue();
            }
        ]);
        $form->add('type', 'choice', [
            'choices' => $repo->findBy(['type' => Property::TYPE_TYPE]),
            'choice_label' => function (Property $property) {
                return $property->getValue();
            }
        ]);
        $form->add('amount', 'integer');
    }

    public function postPersist($object)
    {
        $factory = $this->getJamJarFactory();
        $factory->duplicateJamJar($object, $object->getAmount() - 1);
    }

    /**
     * @return \JamBundle\Factory\JamJarFactory|object
     */
    protected function getJamJarFactory()
    {
        return $this->getConfigurationPool()->getContainer()->get('app.factory.jam_jar');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('year');
        $datagridMapper->add('type');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('type');
        $listMapper->addIdentifier('year');
        $listMapper->addIdentifier('comment');
    }
}
