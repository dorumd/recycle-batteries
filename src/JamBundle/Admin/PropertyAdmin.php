<?php

namespace JamBundle\Admin;

use JamBundle\Entity\Property;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class PropertyAdmin
 * @package JamBundle\Admin
 */
class PropertyAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->add('value');
        $form->add('type', 'choice', [
            'choices' => ['Type' => Property::TYPE_TYPE, 'Year' => Property::TYPE_YEAR],
        ]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('typeFormatted');
        $listMapper->addIdentifier('value');
    }
}