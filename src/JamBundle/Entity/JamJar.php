<?php

namespace JamBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JamJar
 *
 * @ORM\Table(name="jam_jar")
 * @ORM\Entity(repositoryClass="JamBundle\Repository\JamJarRepository")
 */
class JamJar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var Property
     *
     * @ORM\ManyToOne(targetEntity="JamBundle\Entity\Property")
     */
    private $year;

    /**
     * @var Property
     *
     * @ORM\ManyToOne(targetEntity="JamBundle\Entity\Property")
     */
    private $type;

    /**
     * @var int
     */
    private $amount = 1;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return JamJar
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return Property
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return Property
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set year
     *
     * @param \JamBundle\Entity\Property $year
     *
     * @return JamJar
     */
    public function setYear(\JamBundle\Entity\Property $year = null)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Set type
     *
     * @param \JamBundle\Entity\Property $type
     *
     * @return JamJar
     */
    public function setType(\JamBundle\Entity\Property $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;

        return $this;
    }
}
