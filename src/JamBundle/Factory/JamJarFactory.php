<?php

namespace JamBundle\Factory;

use JamBundle\Entity\JamJar;

/**
 * Class JamJarFactory
 */
class JamJarFactory
{
    /** @var \Doctrine\ORM\EntityManagerInterface */
    protected $em;

    /**
     * JamJarFactory constructor.
     * @param \Doctrine\ORM\EntityManagerInterface $em
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createJamJar()
    {
        return new JamJar();
    }

    public function duplicateJamJar(JamJar $jamJar, $copies = 1)
    {
        while ($copies > 0) {
            $jamJarCopy = new JamJar();
            $jamJarCopy->setComment($jamJar->getComment());
            $jamJarCopy->setType($jamJar->getType());
            $jamJarCopy->setYear($jamJar->getYear());

            $this->em->persist($jamJarCopy);

            $copies--;
        }

        $this->em->flush();
    }
}