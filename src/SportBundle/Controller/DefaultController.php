<?php

namespace SportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $data = $this->get('sport.service.exercise')->getDashboardData();

        return $this->render('SportBundle:Default:index.html.twig', [
            'data' => $data,
        ]);
    }
}
