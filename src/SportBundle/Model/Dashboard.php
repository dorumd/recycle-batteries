<?php

namespace SportBundle\Model;

use SportBundle\Entity\Exercise;
use SportBundle\Model\Enum\Periods;

class Dashboard
{
    const DEFAULT_DATE_FORMAT = 'Y-m-d';

    /** @var array */
    private $data;

    /** @var \DateTime */
    private $currentDate;

    /**
     * Dashboard constructor.
     *
     * @param \DateTime $currentDate
     */
    public function __construct(\DateTime $currentDate)
    {
        $this->currentDate = $currentDate;

        foreach (Periods::getPeriods($this->currentDate) as $k => $v) {
            $this->data[$k] = [
                'date' => $v,
                'items' => [],
            ];
        }
    }

    /**
     * @param string $period
     * @param array  $data
     *
     * @throws \Exception
     */
    public function setPeriodData(string $period, array $data)
    {
        $this->validateItems($data, $period);
        $this->data[$period]['items'] = $data;
    }

    protected function validateItems(array $items, string $period)
    {
        if (!isset($this->data[$period])) {
            throw new \Exception('Unrecognized period specified.');
        }

        /** @var Exercise $item */
        foreach ($items as $item) {
            if ($item->getDate()->format(self::DEFAULT_DATE_FORMAT) !== $this->data[$period]['date']->format(self::DEFAULT_DATE_FORMAT)) {
                throw new \Exception('Wrong item date.');
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $period
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getPeriodData(string $period)
    {
        if (!isset($this->data[$period])) {
            throw new \Exception('Undefined period.');
        }

        return $this->data[$period];
    }

    /**
     * @return \DateTime
     */
    public function getCurrentDate()
    {
        return $this->currentDate;
    }
}