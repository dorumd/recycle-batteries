<?php

namespace SportBundle\Model\Enum;

class Periods
{
    const TODAY = 0;
    const ONE_WEEK_AGO = 1;
    const TWO_WEEKS_AGO = 2;

    const ELEMENTS = [
        self::TODAY => 'today',
        self::ONE_WEEK_AGO => 'one_week_ago',
        self::TWO_WEEKS_AGO => 'two_weeks_ago',
    ];

    public static function getPeriods(\DateTime $currentDate)
    {
        $data = [];

        foreach (static::ELEMENTS as $k => $v) {
            $data[$v] = static::getPeriod($k, $currentDate);
        }

        return $data;
    }

    /**
     * @param int       $period
     * @param \DateTime $currentDate
     *
     * @return \DateTime
     *
     * @throws \Exception
     */
    public static function getPeriod(int $period, \DateTime $currentDate)
    {
        $date = clone $currentDate;

        switch ($period) {
            case static::TODAY:
                return $date;
            case static::ONE_WEEK_AGO:
                return $date->sub(new \DateInterval('P1W'));
            case static::TWO_WEEKS_AGO:
                return $date->sub(new \DateInterval('P2W'));
            default:
                throw new \Exception('Unrecognized period specified.');
        }
    }
}
