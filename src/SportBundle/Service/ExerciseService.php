<?php

namespace SportBundle\Service;

use Doctrine\ORM\EntityRepository;
use SportBundle\Model\Dashboard;
use SportBundle\Model\Enum\Periods;

/**
 * Class ExerciseService
 * @package SportBundle\Service
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class ExerciseService
{
    /** @var EntityRepository */
    private $repository;

    /**
     * ExerciseService constructor.
     *
     * @param EntityRepository $repository
     */
    public function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Dashboard
     *
     * @throws \Exception
     */
    public function getDashboardData()
    {
        $dashboard = $this->createDashboard();

        foreach (Periods::ELEMENTS as $k => $v) {
            $dashboard->setPeriodData($v, $this->getExercises(Periods::getPeriod($k, $dashboard->getCurrentDate())));
        }

        return $dashboard;
    }

    /**
     * @param \DateTime $date
     *
     * @return array
     */
    public function getExercises(\DateTime $date)
    {
        $exercises = $this->repository->findBy(['date' => $date]);

        return $exercises;
    }

    /**
     * @return Dashboard
     */
    protected function createDashboard()
    {
        $currentDate = new \DateTime();

        return new Dashboard($currentDate);
    }
}
