<?php

namespace StudentBundle\Command;

use StudentBundle\Entity\Student;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StudentUpdatePathCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('student:update-path')
            ->setDescription('...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        gc_enable();
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $slugs = [];
        $ids = [];
        $q = sprintf('update %s s set s.path = CASE id', 'student');

        $time_start = microtime(true);

        $batchSize = 1000;
        $limit = 1000;
        $offset = 0;
        gc_enable();
        for ($i = 1; $i <= 25; ++$i) {
            $students = $em->getRepository(Student::class)->findByPaginated($limit, $offset);

            foreach ($students as $student) {
                $slug = $this->generateSlug($student['name']);
                if (!isset($slugs[$slug])) {
                    $slugs[$slug] = [];
                }
                $slugs[$slug][] = $student['id'];
            }

            $offset = $batchSize * $i;
            $em->flush();
            $em->clear();
            gc_collect_cycles();
        }

        foreach ($slugs as $k => $v) {
            $i = 0;
            if (count($v) > 1) {
                foreach ($v as $id) {
                    $slug = $k;
                    if ($i !== 0) {
                        $slug .= '-' . $i;
                    }
                    $q .= ' WHEN ' . $id . ' THEN \'' . $slug  . '\'';
                    $i++;
                    $ids[] = $id;
                }
            } else {
                $ids[] = $v['0'];
                $q .= ' WHEN ' . $v['0'] . ' THEN \'' . $k . '\'';
            }
        }
        $q .= ' END ';
        $q .= 'WHERE id in (' .  implode(',', $ids) . ')';

        $query = $em->getConnection()->prepare($q);
        $query->execute();

        echo 'Total execution time in seconds: ' . (microtime(true) - $time_start) . '\n';
        $output->writeln('Memory peak usage: ' . memory_get_peak_usage()/1024/1024);
    }

    public function generateSlug($name)
    {
        $slug = str_replace(' ', '-', strtolower($name));

        return self::slugify($name);
    }

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
