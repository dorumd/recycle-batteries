<?php

namespace StudentBundle\Command;

use StudentBundle\Entity\Student;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StudentUpdatePathIteratorCommand extends ContainerAwareCommand
{
    const BATCH_SIZE = 1000;

    protected function configure()
    {
        $this
            ->setName('student:update-path-iterator')
            ->setDescription('La la la')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        gc_enable();
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $time_start = microtime(true);

        $service = $this->getContainer()->get('student.service');

        /** @var Student[] $students */
        $students = $em->getRepository(Student::class)->findIterator();

        $i = 0;
        foreach ($students as list($student)) {
            $student->setPath($service->generateSlug($student->getName()));

            ++$i;

            if ($i % self::BATCH_SIZE === 0) {
                $em->flush();
                $em->clear();
                gc_collect_cycles();
            }
        }

        $output->writeln('Total execution time in seconds: ' . (microtime(true) - $time_start));
        $output->writeln('Memory peak usage: ' . memory_get_peak_usage()/1024/1024);
    }

}
