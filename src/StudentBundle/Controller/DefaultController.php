<?php

namespace StudentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use StudentBundle\Entity\Student;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class DefaultController
 * @package StudentBundle\Controller
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class DefaultController extends Controller
{
    /**
     * @Route("/students/detail/{path}")
     * @Cache(maxage=900, public=true)
     * @ParamConverter("student", options={"mapping": {"path": "path"}})
     */
    public function indexAction(Student $student)
    {
        return $this->render('StudentBundle:Default:index.html.twig', [
            'student' => $student,
        ]);
    }
}
