<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18/07/17
 * Time: 06:13
 */

namespace StudentBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use StudentBundle\Entity\Student;

class LoadStudentsData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setPassword('test');

        $manager->persist($userAdmin);
        $manager->flush();
    }
}