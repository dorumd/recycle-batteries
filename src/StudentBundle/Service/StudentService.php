<?php

namespace StudentBundle\Service;


use Doctrine\ORM\EntityManagerInterface;

class StudentService
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var array */
    protected $cache;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->cache = [];
    }

    public function generateSlug($slug)
    {
        $slug = static::sanitizeSlug($slug);

        if (isset($this->cache[$slug])) {
            $i = 0;
            do {
                $updatedSlug = $slug . '-' . $i;
                ++$i;
            } while (isset($this->cache[$updatedSlug]));
            $slug = $updatedSlug;
        }

        $this->cache[$slug] = true;

        return $slug;
    }

    static public function sanitizeSlug($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}