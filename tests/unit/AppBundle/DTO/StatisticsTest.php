<?php

namespace tests\unit\AppBundle\DTO;
use AppBundle\DTO\Statistics;
use AppBundle\Entity\Enum\BatteryTypeEnum;

/**
 * Class StatisticsTest
 * @package tests\unit\AppBundle\DTO
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class StatisticsTest extends \PHPUnit_Framework_TestCase
{
    /** @var Statistics */
    protected $statistics;

    public function setUp()
    {
        $this->statistics = new Statistics();
    }

    public function tearDown()
    {
        $this->statistics = null;
    }

    /**
     * @test
     */
    public function itWasInitialisedCorrectly()
    {
        $expectedCount = 0;

        foreach (BatteryTypeEnum::ELEMENTS as $key => $label) {
            $this->assertArrayHasKey($key, $this->statistics->getData());
            $this->assertArrayHasKey('label', $this->statistics->getData()[$key]);
            $this->assertArrayHasKey('count', $this->statistics->getData()[$key]);

            $this->assertEquals($expectedCount, $this->statistics->getData()[$key]['count']);
            $this->assertEquals($label, $this->statistics->getData()[$key]['label']);
        }
    }

    /**
     * @test
     */
    public function addWorksCorrectly()
    {
        $this->statistics->add(BatteryTypeEnum::TYPE_AA, 3);
        $this->assertEquals(3,$this->statistics->getData()[BatteryTypeEnum::TYPE_AA]['count']);

        $this->statistics->add(BatteryTypeEnum::TYPE_AA, 3);
        $this->assertEquals(6,$this->statistics->getData()[BatteryTypeEnum::TYPE_AA]['count']);
    }

    /**
     * @test
     *
     * @expectedException \Exception
     */
    public function addThrowsAnExceptionForUnexpectedTypes()
    {
        $this->statistics->add(1999, 10);
    }


    /**
     * @test
     */
    public function removeWorksCorrectly()
    {
        $this->statistics->remove(BatteryTypeEnum::TYPE_AA, 3);
        $this->assertEquals(-3, $this->statistics->getData()[BatteryTypeEnum::TYPE_AA]['count']);

        $this->statistics->remove(BatteryTypeEnum::TYPE_AA, 3);
        $this->assertEquals(-6, $this->statistics->getData()[BatteryTypeEnum::TYPE_AA]['count']);
    }

    /**
     * @test
     *
     * @expectedException \Exception
     */
    public function removeThrowsAnExceptionForUnexpectedTypes()
    {
        $this->statistics->remove(1999, 10);
    }
}
