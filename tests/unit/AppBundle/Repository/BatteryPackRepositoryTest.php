<?php

namespace tests\unit\AppBundle\Repository;
use AppBundle\Entity\BatteryPack;
use AppBundle\Repository\BatteryPackRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BatteryPackRepositoryTest
 * @package tests\unit\AppBundle\Repository
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class BatteryPackRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var BatteryPackRepository|\PHPUnit_Framework_MockObject_MockObject */
    protected $repository;

    /** @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $em;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(BatteryPackRepository::class)
            ->disableOriginalConstructor()
            ->setMethodsExcept(['add', 'remove'])
            ->getMock();

        $this->em = $this->createMock(EntityManagerInterface::class);

        $this->repository->method('getManager')->willReturn($this->em);
    }

    public function tearDown()
    {
        $this->repository = null;
        $this->em = null;
    }

    /**
     * @test
     */
    public function add()
    {
        /** @var BatteryPack|\PHPUnit_Framework_MockObject_MockObject $batteryPackMock */
        $batteryPackMock = $this->createMock(BatteryPack::class);

        $this->em->expects($this->once())
            ->method('persist')
            ->withAnyParameters()
            ->willReturn($this->any())
        ;

        $this->em->expects($this->once())
            ->method('flush')
            ->withAnyParameters()
            ->willReturn($this->any())
        ;

        $this->repository->add($batteryPackMock);
    }

    /**
     * @test
     */
    public function remove()
    {
        /** @var BatteryPack|\PHPUnit_Framework_MockObject_MockObject $batteryPackMock */
        $batteryPackMock = $this->createMock(BatteryPack::class);

        $this->em->expects($this->once())
            ->method('remove')
            ->withAnyParameters()
            ->willReturn($this->any())
        ;

        $this->em->expects($this->once())
            ->method('flush')
            ->withAnyParameters()
            ->willReturn($this->any())
        ;

        $this->repository->remove($batteryPackMock);
    }
}
