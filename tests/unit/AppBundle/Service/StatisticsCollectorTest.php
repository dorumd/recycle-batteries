<?php

namespace tests\unit\AppBundle\Service;

use AppBundle\Entity\Enum\BatteryTypeEnum;
use AppBundle\Repository\BatteryPackRepository;
use AppBundle\Service\StatisticsCollector;

/**
 * Class StatisticsCollectorTest
 * @package tests\unit\AppBundle\Service
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class StatisticsCollectorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var StatisticsCollector
     */
    protected $statisticsCollector;

    /**
     * @var BatteryPackRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $batteriesPackRepository;

    public function setUp()
    {
        $this->batteriesPackRepository = $this->getMockBuilder(BatteryPackRepository::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->statisticsCollector = new StatisticsCollector($this->batteriesPackRepository);
    }

    public function tearDown()
    {
        $this->batteriesPackRepository = null;
        $this->statisticsCollector = null;
    }

    /**
     * @test
     */
    public function collect()
    {
        $data = [];
        foreach (BatteryTypeEnum::ELEMENTS as $type => $label) {
            for ($i = 0; $i < 3; $i++) {
                $data[] = ['type' => $type, 'totalAmount' => $i + 1];
            }
        }

        $this->batteriesPackRepository->expects($this->once())
            ->method('findCountOfType')
            ->withAnyParameters()
            ->willReturn($data)
        ;

        $this->statisticsCollector->collect();
        $this->assertEquals(true, $this->statisticsCollector->isCollected());

        /** @var array $result */
        $result = $this->statisticsCollector->getCollectedData();

        foreach (BatteryTypeEnum::ELEMENTS as $type => $label) {
            $this->assertArrayHasKey($type, $result);
            $this->assertEquals(6, $result[$type]['count']);
            $this->assertEquals($label, $result[$type]['label']);
        }
    }
}
