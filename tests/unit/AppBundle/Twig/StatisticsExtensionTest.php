<?php

namespace tests\unit\AppBundle\Twig;

use AppBundle\Service\StatisticsCollector;
use AppBundle\Twig\StatisticsExtension;

/**
 * Class StatisticsExtensionTest
 * @package tests\unit\AppBundle\Twig
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class StatisticsExtensionTest extends \PHPUnit_Framework_TestCase
{
    /** @var StatisticsExtension */
    protected $statisticsExtension;

    /** @var StatisticsCollector|\PHPUnit_Framework_MockObject_MockObject */
    protected $statisticsCollector;

    /** @var \Twig_Environment|\PHPUnit_Framework_MockObject_MockObject */
    protected $twigEnvironment;

    public function setUp()
    {
        $this->statisticsCollector = $this->getMockBuilder(StatisticsCollector::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
        $this->twigEnvironment = $this->getMockBuilder(\Twig_Environment::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->statisticsExtension = new StatisticsExtension($this->statisticsCollector);
    }

    public function tearDown()
    {
        $this->statisticsCollector = null;
        $this->statisticsExtension = null;
    }

    /**
     * @test
     */
    public function renderStatistics()
    {
        $this->twigEnvironment->expects($this->once())
            ->method('render')
            ->with('common/_statistics.html.twig', ['statistics' => []])
            ->willReturn('')
        ;

        $this->statisticsExtension->renderStatistics($this->twigEnvironment);
    }
}
