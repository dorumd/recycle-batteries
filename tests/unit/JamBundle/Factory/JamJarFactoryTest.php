<?php

namespace tests\unit\JamBundle\Factory;

use Doctrine\ORM\EntityManagerInterface;
use JamBundle\Entity\JamJar;
use JamBundle\Factory\JamJarFactory;

/**
 * Class JamJarFactoryTest
 * @package tests\unit\JamBundle\Factory
 *
 * @group unit-jam
 */
class JamJarFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $em;

    /** @var JamJarFactory|\PHPUnit_Framework_MockObject_MockObject */
    protected $factory;

    protected function setUp()
    {
        $this->em = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->factory = $this->getMockBuilder(JamJarFactory::class)
            ->setConstructorArgs([$this->em])
            ->setMethods(['createNew'])
            ->getMock();
    }

    protected function tearDown()
    {
        $this->em = null;
    }

    /**
     * @test
     */
    public function duplicateJamJar()
    {
        $amount = 10;
        $jamJar = new JamJar();

        $this->em->expects($this->exactly($amount))
            ->method('persist')
            ->withAnyParameters()
            ->willReturn($this->any());

        $this->em->expects($this->once())
            ->method('flush')
            ->withAnyParameters()
            ->willReturn($this->any());

        $this->factory->duplicateJamJar($jamJar, $amount);
    }
}
