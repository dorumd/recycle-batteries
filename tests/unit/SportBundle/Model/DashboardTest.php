<?php

namespace tests\unit\SportBundle\Model;

use SportBundle\Entity\Exercise;
use SportBundle\Model\Dashboard;
use SportBundle\Model\Enum\Periods;

/**
 * Class DashboardTest
 * @package tests\unit\SportBundle\Model
 *
 * @group unit-sport
 */
class DashboardTest extends \PHPUnit_Framework_TestCase
{
    /** @var \DateTime */
    protected $currentDate;

    /**
     * Setup
     */
    protected function setUp()
    {
        $this->currentDate = new \DateTime();
    }

    /**
     * Tear down
     */
    protected function tearDown()
    {
        $this->currentDate = null;
    }

    /**
     * @test
     */
    public function isInstantiatedCorrectly()
    {
        $dashboard = new Dashboard($this->currentDate);

        $this->assertEquals(count(Periods::getPeriods($this->currentDate)), count($dashboard->getData()));

        foreach (Periods::getPeriods($this->currentDate) as $k => $v) {
            $this->assertArrayHasKey($k, $dashboard->getData());
        }
    }

    /**
     * @test
     *
     * @expectedException \Exception
     * @expectedExceptionMessage Undefined period.
     */
    public function getPeriodDataForUndefinedPeriod()
    {
        $dashboard = new Dashboard($this->currentDate);

        $dashboard->getPeriodData('dasdasdas');
    }

    /**
     * @test
     */
    public function getPeriodDataForExistingPeriod()
    {
        $dashboard = new Dashboard($this->currentDate);
        $result = $dashboard->getPeriodData(Periods::ELEMENTS[Periods::TODAY]);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('date', $result);
        $this->assertArrayHasKey('items', $result);
    }

    /**
     * @test
     */
    public function getData()
    {
        $dashboard = new Dashboard($this->currentDate);
        $result = $dashboard->getData();

        $this->assertInternalType('array', $result);
    }


    /**
     * @test
     *
     * @expectedException \Exception
     * @expectedExceptionMessage Unrecognized period specified.
     */
    public function setPeriodDataForUndefinedPeriod()
    {
        $dashboard = new Dashboard($this->currentDate);

        $dashboard->setPeriodData('dasdasdas', []);
    }

    /**
     * @test
     */
    public function setPeriodDataForExistingPeriod()
    {
        $period = Periods::ELEMENTS[Periods::TODAY];

        $dashboard = new Dashboard($this->currentDate);
        $dashboard->setPeriodData($period, []);
        $result = $dashboard->getPeriodData($period);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('date', $result);
        $this->assertArrayHasKey('items', $result);
    }


    /**
     * @test
     *
     * @expectedException \Exception
     * @expectedExceptionMessage Wrong item date.
     */
    public function setPeriodDataForExistingPeriodWithWrongExercise()
    {
        $period = Periods::ELEMENTS[Periods::TWO_WEEKS_AGO];
        $items = [];

        $exercise = new Exercise();
        $exercise->setDate(new \DateTime());

        $items[] = $exercise;

        $dashboard = new Dashboard($this->currentDate);
        $dashboard->setPeriodData($period, $items);
    }
}
