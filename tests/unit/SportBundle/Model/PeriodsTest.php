<?php

namespace tests\unit\SportBundle\Model;

use SportBundle\Model\Enum\Periods;

/**
 * Class PeriodsTest
 * @package tests\unit\SportBundle\Model
 *
 * @group unit-sport
 */
class PeriodsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getPeriods()
    {
        $date = \DateTime::createFromFormat('Y-m-d', '2017-02-15');

        $result = Periods::getPeriods($date);

        $this->assertInternalType('array', $result);
        $this->assertCount(3, $result);
        $this->assertArrayHasKey('today', $result);
        $this->assertArrayHasKey('one_week_ago', $result);
        $this->assertArrayHasKey('two_weeks_ago', $result);

        $this->assertEquals('2017-02-15', $result['today']->format('Y-m-d'));
        $this->assertEquals('2017-02-08', $result['one_week_ago']->format('Y-m-d'));
        $this->assertEquals('2017-02-01', $result['two_weeks_ago']->format('Y-m-d'));
    }

    /**
     * @test
     */
    public function getPeriod()
    {
        $date = new \DateTime();

        $result = Periods::getPeriod(Periods::TODAY, $date);

        $this->assertInstanceOf('DateTime', $result);
        $this->assertEquals($date, $result);
    }
}
