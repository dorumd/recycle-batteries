<?php

namespace tests\unit\SportBundle\Service;

use Doctrine\ORM\EntityRepository;
use SportBundle\Model\Dashboard;
use SportBundle\Model\Enum\Periods;
use SportBundle\Service\ExerciseService;

/**
 * Class ExerciseServiceTest
 * @package tests\unit\SportBundle\Service
 *
 * @group unit-sport
 */
class ExerciseServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var ExerciseService */
    private $exerciseService;

    /** @var EntityRepository|\PHPUnit_Framework_MockObject_MockObject */
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(EntityRepository::class)->disableOriginalConstructor()->getMock();

        $this->exerciseService = new ExerciseService($this->repository);
    }

    public function tearDown()
    {
        $this->repository = null;
        $this->exerciseService = null;
    }

    /**
     * @test
     */
    public function getExercises()
    {
        $date = new \DateTime();
        $exercises = [];

        $this->repository
            ->expects($this->once())
            ->method('findBy')
            ->with(['date' => $date])
            ->willReturn($exercises)
        ;

        $result = $this->exerciseService->getExercises($date);
        $this->assertEquals($exercises, $result);
    }

    /**
     * @test
     */
    public function getDashboardData()
    {
        $this->repository
            ->expects($this->exactly(3))
            ->method('findBy')
            ->withAnyParameters()
            ->willReturn([])
        ;

        $result = $this->exerciseService->getDashboardData();

        $this->assertInstanceOf(Dashboard::class, $result);

        $data = $result->getData();
        $this->assertEquals(count(Periods::ELEMENTS), count($data));

        foreach (Periods::ELEMENTS as $k => $v) {
            $this->assertArrayHasKey($v, $data);
        }
    }
}
