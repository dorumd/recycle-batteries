<?php

namespace tests\unit\StudentBundle\Service;
use Doctrine\ORM\EntityManagerInterface;
use StudentBundle\Service\StudentService;

/**
 * Class StudentServiceTest
 * @package tests\unit\StudentBundle\Service
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 */
class StudentServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $em;

    /** @var StudentService */
    protected $studentService;

    protected function setUp()
    {
        $this->em = $this->createMock(EntityManagerInterface::class);

        $this->studentService = new StudentService($this->em);
    }

    protected function tearDown()
    {
        $this->em = null;
        $this->studentService = null;
    }

    /**
     * @test
     */
    public function generateSlug()
    {
        $name = 'Mostafa Alibekov';

        $result1 = $this->studentService->generateSlug($name);
        $result2 = $this->studentService->generateSlug($name);
        $result3 = $this->studentService->generateSlug($name);

        $this->assertEquals('mostafa-alibekov', $result1);
        $this->assertEquals('mostafa-alibekov-0', $result2);
        $this->assertEquals('mostafa-alibekov-1', $result3);
    }
}
